import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:qrscanner/qr_screen_overlay.widget.dart';

class QRScannerWidget extends StatefulWidget {
  const QRScannerWidget({super.key});

  @override
  State<QRScannerWidget> createState() => _QRScannerWidgetState();
}

class _QRScannerWidgetState extends State<QRScannerWidget> {
  MobileScannerController cameraController = MobileScannerController();

  bool isScanCompleted = false;
  String result = "";

  void closeScreen() {
    isScanCompleted = false;
  }

  @override
  void initState() {
    super.initState();
    isScanCompleted = false;
  }

  @override
  void dispose() {
    isScanCompleted = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return SafeArea(
      child: Scaffold(
        body: !isScanCompleted
            ? SizedBox(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 8,
                      child: Stack(
                        children: [
                          MobileScanner(
                            controller: cameraController,
                            startDelay: true,
                            onDetect: (capture) {
                              if (!isScanCompleted) {
                                final List<Barcode> barcodes = capture.barcodes;
                                for (final barcode in barcodes) {
                                  result = barcode.rawValue ?? '';
                                }
                                isScanCompleted = true;
                                setState(() {});
                              }
                            },
                          ),
                          const QRScannerOverlayWidget(
                            bg: Colors.transparent,
                            bgBorder: Colors.greenAccent,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: SizedBox(
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Coloca el QR en el area",
                              style: textTheme.titleMedium,
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              "El scaneo se hara de forma automatica",
                              style: textTheme.bodyMedium,
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ),

                    // Expanded(
                    //   child: Container(
                    //     alignment: Alignment.center,
                    //     child: Text(
                    //       "Desarrollado por Azurian",
                    //       style: textTheme.bodySmall,
                    //       textAlign: TextAlign.center,
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              )
            : Text(result),
      ),
    );
  }
}
